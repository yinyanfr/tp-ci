// Dépendances
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const fonctions = require('./fonctions')

// Moteur de template
app.set('view engine', 'ejs')

// Middlewares
app.use('/assets', express.static('public'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Routes
app.get('/', function (req, res) {
	res.render('index', {resultat: undefined, nombre: undefined})
})

app.post('/', function (req, res) {
	let nombre = parseInt(req.body.nombre)
	console.log(req.body.nombre)
	let resultat = undefined
	try {
		resultat = fonctions.factorielle(nombre)
		res.json({resultat: resultat, nombre: nombre})
	} catch(error) {
		console.log(error)
		res.json({error: 'Le nombre ne peut être négatif'})
	}
	
})

// Lancement du serveur
app.listen(80, function () {
	console.log('App listening on port 80')
})

module.exports = app
