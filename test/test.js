var assert = require('assert')
var expect = require('chai').expect
var fonctions = require('../fonctions')
const request = require("supertest")
const app = require("../index")

describe('Tests unitaires', function() {
	describe('Fonction factorielle', function() {

		it('should throw an exception when the value is negative', function() {
			expect(() => {fonctions.factorielle(-1)}).to.throw('Cannot be negative')
		})

		it('should return 1 when the value is equal to 0', function() {
			assert.equal(fonctions.factorielle(0), 1)
		})
		
		it('should return 1 when the value is equal to 1', function() {
			assert.equal(fonctions.factorielle(1), 1)
		})

		it('should return 2 when the value is equal to 2', function() {
			assert.equal(fonctions.factorielle(2), 2)
		})

		it('should return 6 when the value is equal to 3', function() {
			assert.equal(fonctions.factorielle(3), 6)
		})
	})
})

describe('POST "/"', () => {
	it("should return its fac", async () => {
		const nombre = 45
		const res = await request(app)
			.post("/")
			.send({nombre})
			.expect(200)

		const {resultat} = res.body
		assert.equal(resultat, fonctions.factorielle(nombre))
	})
})
